import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {

    private final int[] input;
    private final int[] expected;

    public SortingAppTest(int[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[0], new int[0]},                            // Zero arguments
                {new int[]{5}, new int[]{5}},                        // One argument
                {new int[]{9, 4, 7, 2, 1}, new int[]{1, 2, 4, 7, 9}}, // Random values
                {new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1},           // Ten arguments
                        new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new int[]{13, 6, 20, 3, 9, 12, 17, 8, 1, 15, 5, 10}, // More than 10 arguments
                        new int[]{1, 3, 5, 6, 8, 9, 10, 12, 13, 15, 17, 20}}
        });
    }

    @Test
    public void testSortingApp() {
        int[] result = SortingApp.sort(input);
        assertArrayEquals(expected, result);
    }
}