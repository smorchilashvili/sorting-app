import java.util.Arrays;

public class SortingApp {

    public static void main(String[] args) {

        // Parse command-line arguments as integers
        int[] inputNumbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                inputNumbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.err.println("Invalid input: " + args[i]);
                System.exit(1);
            }
        }

        // Check if there are more than 10 arguments
        if (inputNumbers.length > 10) {
            System.err.println("Too many arguments. Maximum allowed is 10.");
            System.exit(1);
        }

        int[] sortedNumbers = SortingApp.sort(inputNumbers);

        // Print the sorted numbers
        for (int number : sortedNumbers)
            System.out.println(number);
    }

    public static int[] sort(int[] numbers) {
        Arrays.sort(numbers);
        return numbers;
    }
}